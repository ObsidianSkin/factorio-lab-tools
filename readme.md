## Prepare

1. Download and unpack _tcc-0.9.27-win64-bin.zip_ and _winapi-full-for-0.9.27.zip_ from http://download.savannah.gnu.org/releases/tinycc/
2. Update TCC's .def-files:

```
cd /d %TCC_DIR%\lib
..\tcc.exe -impdef kernel32.dll
..\tcc.exe -impdef user32.dll
..\tcc.exe -impdef msvcrt.dll
..\tcc.exe -impdef ws2_32.dll
```

## Build on Windows

Edit mass_make.cmd and set:

* `CC` - path to TCC x64 compiler
* `V=1` - show full command output
* `DIST` - install path
* `LUAVER` - Lua version (53 or 52f)

_Note: Lua 52f is forked from https://github.com/Rseding91/Factorio-Lua_

## Usage (for KirkMcDonald calculator)

Edit *\[lua-kirk/\]factorio_options.lua* and set:

* `game_path` = path to Factorio dir
* `mod_path` = path to mod dir
* `calcDir` = path to *KirkMcDonald/factorio-web-calc* (defaut is *web-calc*)

Run `lua factorio_calc.lua` and open http://127.0.0.1:8080 in browser.

You can run `lua factorio_dump.lua` for convert game data to _**prefix**-**version**.json_ and _sprite-sheet-**hash**.png_ saved to _**calcDir**/data_ and _**calcDir**/images_.


## Usage (for Factorio Lab)

Build (or download ready package from [Downloads](https://bitbucket.org/hhrhhr/factorio-lab-tools/downloads/) )

```
Usage: [lua-lab/]factorio_data_dump.lua [-h] [-g <gamedir>] [-m <moddir>]
                              [-s <iconsize>] [-f <suffix>] [-n] [-i]
                              [-l <language>] [-c]
                              [--factorio_lab_hacks] [-b] [-v] [-d]
                              <command> ...

Data exporter for Factorio.

Options:
   -h, --help                show this help message and exit
   -v, --verbose             more verbose (try -vvv)
   -g, --gamedir <gamedir>   game location (default: .)
   -m, --moddir <moddir>     override mods location
   -s, --iconsize <iconsize> icon size (default: 32)
   -f, --suffix <suffix>     a string that is added to the file name (default: "")
   -n, --nomods              disable mods
   -i, --noimage             disable image generation
   -l, --language <language> select localization (default: en)
   -c, --clear               clear unneded fields in data.raw
   --factorio_lab_hacks      use special hacks for Factorio Lab
   -b, --browse              * open browser (only with 'calc' command)
   -d, --debug               * start mobdebug

Commands:
   dump                  export data.raw from game and save it
   export                export data.raw for Factorio Lab
   demo                  export data.raw for demo page generation (with demo.lua)
   web                   * just start web server

* - not implemented
```

### Issues

* Only unpacked modifications are supported.
* The selection of the used modifications is exported from the game, as well as their settings (so first start the game, select the mods, configure them as you wish and exit the game).
* Some recipes show unsupported machines (e.g. "uranium ore" should not be mined with a "burner mining drill")
