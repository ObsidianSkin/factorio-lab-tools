local gd = require("gd")

local dbglog, args, mods

local function apply_tint(ico, tint)
    local X, Y = ico:sizeXY() -- gd func
    for y = 0, X-1 do
        for x = 0, Y-1 do
            local c = ico:getPixel(x, y)
            local r = math.floor(ico:red(c)   * tint.r)
            local g = math.floor(ico:green(c) * tint.g)
            local b = math.floor(ico:blue(c)  * tint.b)
--            local a = math.floor(ico:alpha(c) * tint.a)
            local a = ico:alpha(c)
            c = ico:colorAllocateAlpha(r, g, b, a)
            ico:setPixel(x, y, c)
        end
    end
end


local function copy_icon(x, y, SZ, mult, icon, canvas, scale2)
    local dstX = x * SZ - SZ
    local dstY = y * SZ - SZ

    -- draw multi-layers images
    local layers = 0

    for _, v in ipairs(icon.path) do
        layers = layers + 1

        local png
        if canvas then
            local path = v.icon:gsub("__(.+)__", mods)
            png = gd.createFromPng(path)
            png:saveAlpha(true)
        end

        -- apply tint
        if v.tint and canvas then
            apply_tint(png, v.tint)
        end

        local size = v.size or 32
        local scale = v.scale and ((v.scale * size) / (32*mult)) or 1.0

        -- move scaled icon to center
        local dc = SZ * 0.5 * (1.0 - scale)

        -- shift scaled icon
        local vx, vy = 0, 0
        if v.shift then
            vx = v.shift[1] * (SZ / 64)
            vy = v.shift[2] * (SZ / 64)
        end
        local dx = math.floor(dc + vx)
        local dy = math.floor(dc + vy)

        local dstW = math.floor(SZ * scale * (scale2 or 1.0))
        local srcW = size

        -- try to select right mipmap
        local shiftX = 0
--[[ -- not now ;)
        local mips = v.mips
        while srcW > SZ and mips > 1 do
            shiftX = shiftX + srcW
            srcW = srcW / 2
            mips = mips - 1
        end
]]
        if canvas then
            canvas:copyResampled(
--            canvas:copyResized(
                png, dstX+dx, dstY+dy, shiftX, 0, dstW, dstW, srcW, srcW)
        end
--        break
    end

    return dstX, dstY, layers 
end


local function generate_image(icons)
    local SZ = args.iconsize
    local image_w, image_h, idx, canvas, image_fn

    -- calculate size of icons
    local num = #icons
    image_w = math.ceil(num^0.5)
    if image_w < 2 then
        image_w = 2
    end
    image_h = math.ceil(num / image_w)
    if image_h < 2 then
        image_h = 2
    end

    dbglog(1, ("icon's canvas %sx%s (|xx| show number of layers)...\n"):format(image_w, image_h))

    if not args.noimage then
        canvas = gd.createTrueColor(image_w*SZ, image_h*SZ)
        canvas:fill(0, 0, gd.TRANSPARENT)
        canvas:saveAlpha(true)
    end

    local out = {}
    image_fn = ("%s%s.png"):format(args.version, args.suffix)

    for y = 1, image_h do
        dbglog(-1, ("row %3d |"):format(y))
        local icon
        for x = 1, image_w do
            idx, icon = next(icons, idx)
            if not icon then break end

            local scale2 = icon.scale2 or 1.0
            local dstX, dstY, layers = copy_icon(x, y, SZ, 1, icon, canvas, scale2)

            local t = {}
            t.id = icon.id
            t.position = ("%dpx %dpx"):format(-dstX, -dstY)
            t.size = SZ * scale2 --("size-%d"):format(SZ)
            --t.file = "assets/0.18/" .. image_fn
            table.insert(out, t)

            dbglog(-1, ("%2d|"):format(layers))
        end -- for x
        dbglog(-1, "\n")
        if not icon then break end
    end -- for y

    if canvas then
        image = canvas:png(image_fn)
    end

    return out
end

return function(opt)
    dbglog = opt.dbglog
    args = opt.args
    mods = opt.mods
    return { generate_image = generate_image, }
end
