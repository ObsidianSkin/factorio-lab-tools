factorio_calc = true
local dump = require "factorio_dump"
factorio_calc = nil

local xavante = require "xavante"
local hfile = require "xavante.filehandler"
local hredir = require "xavante.redirecthandler"
local httpd = require "xavante.httpd"

local serpent = require "serpent"

local server_html = {
    [1] = "<html><head>",
    [2] = [[<meta http-equiv="Refresh" content="3; URL=calc.html#tab=settings">]],
    [3] = "</head></body>",
    [4] = "<p>Restart in 3 seconds...<p>",
    [5] = "</body></html>",
}
local body = {["/server.html"] = {[2] = "text/html"}}

local need_stop = false
local need_restart = true

local function getServerBody()
    local b = body["/server.html"]
    if not need_restart then
        server_html[2] = ""
        server_html[4] = "<p>Bye...<p>"
    end
    b[1] = table.concat(server_html)
end

local function override(req, res, cap)
    local p = req.parsed_url.path
print("over", p)
    if "/server.html" == p then
        local q = req.parsed_url.query
        local k, v = q:match("(%a+)=(%a+)")
        if "action" == k then
            if "restart" == v then
                need_restart = true
                need_stop = true
                getServerBody()
            elseif "stop" == v then
                need_restart = false
                need_stop = true
                getServerBody()
            else
                print("unknown command " .. v)
                return
            end
        else
            print("unknonw action " .. k)
        end
    end

    res.headers = {}

    local b = body[p]
    if b then
        res.headers["Content-type"] = b[2]
        if b[3] then
            res.headers["Content-Encoding"] = b[3]
        end
        res.headers["Content-Length"] = #b[1]
        res.content = b[1]
        res:send_headers()
    else
        return httpd.err_404(req, res)
    end
end

local function main()
    local options = require("factorio_options") 
    local d = dump.init(false, options)

    local imagePath = d.SpriteHash .. ".png"

    local simplerules = {
        {
            match = "^[^%./]*/$",
            with = hredir,
            params = {"calc.html"}
        },
        {
            match = {"/override.js", "/local-.+.json", imagePath, "/server.html"},
            with = function(...) return override(...) end
        },
        {
            match = ".",
            with = hfile,
            params = {baseDir = options.calcDir}
        },
    }

    xavante.HTTP{
        server = {host = "127.0.0.1", port = 8080},
        defaultHost = {
            rules = simplerules
        }
    }

    body["/override.js"] = {d.Override, "text/javascript"}
    body["/data/local-"..d.Version..".json"] = {d.Normal, "text/json", "deflate"}
    body["/data/local-"..d.Version.."-expensive.json"] = {d.Expensive, "text/json", "deflate"}
    body["/images/sprite-sheet-" .. imagePath] = {d.SpriteSheet, "image/png"}
    d = nil

    while need_restart do
        need_stop = false
        xavante.start(function() return need_stop end)
    end
end

main()
